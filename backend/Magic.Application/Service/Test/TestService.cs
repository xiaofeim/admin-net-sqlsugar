﻿using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Magic.Application.Service
{
    [ApiDescriptionSettings("Application",Name = "Test", Order = 1)]
    public class TestService : ITestService, IDynamicApiController, ITransient
    {
        private readonly SqlSugarRepository<Documentation> _userRep;

        public TestService(SqlSugarRepository<Documentation> userRep)
        {
            _userRep=userRep;
        }

        [HttpGet("/test/TestChangeDatabase")]
        [AllowAnonymous]
        public void TestChangeDatabase()
        {
            var user = _userRep.ToList();
            _userRep.Context.ChangeDatabase("1");
            var user2 = _userRep.ToList();

        }
    }
}
